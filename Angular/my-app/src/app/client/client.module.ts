 
import { NgModule } from '@angular/core';
import { ClientRoutingModule } from './client-routing.module';
import { ClientLayoutComponent } from './layout/client-layout.component';

@NgModule({
  declarations: [
    ClientLayoutComponent
  ],
  imports: [
    ClientRoutingModule
  ],
  providers: [],
  bootstrap: [ ]
})
export class ClientModule { }

