import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientLayoutComponent } from './layout/client-layout.component';

 
 
 

const routes: Routes = [
    
  
    {
      path: '',
      component: ClientLayoutComponent
 
    }

  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class ClientRoutingModule { }
  