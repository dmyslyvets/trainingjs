import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import {TestComponent } from './TEST/test.component'
import { ClientLayoutComponent } from './client/layout/client-layout.component';
import { LoginComponent } from './auth/login/login.component';
 

const routes: Routes = [
    { path: '', redirectTo: 'admin', pathMatch: 'full' },
  
    {
      path: 'client',
      children: [
        { path: '',  loadChildren: () => import("src/app/client/client.module").then(m => m.ClientModule) },
      ]
       
    },

    {
      path: 'login',
      component: LoginComponent
    },
//ClientModule
    {
      path: 'admin',
      children: [
        { path: '',  loadChildren: () => import("src/app/admin/admin.module").then(m => m.AdminModule) },
      ]
       
    }

  ];
  
  @NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: false})],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
  