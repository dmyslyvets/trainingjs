import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TestComponent } from './TEST/test.component';
import { MatSliderModule } from '@angular/material/slider';
import { AppRoutingModule } from "./routing.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { LoginComponent } from './auth/login/login.component';
 
@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatSliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
