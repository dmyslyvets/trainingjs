 
import { NgModule } from '@angular/core';
import { AdminLayoutComponent } from './layout/admin-layout.component';
import { MaterialModule } from './material-module';
import { AdminRoutingModule } from './admin-routing.module';
import { MatNativeDateModule } from '@angular/material/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ClientComponent } from './pages/client/client.component';
import { ProductComponent } from './pages/product/product.component';
 
 
@NgModule({
  declarations: [
    AdminLayoutComponent,
    ClientComponent,
    ProductComponent
     
  ],
  imports: [
    AdminRoutingModule,
    MatNativeDateModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule 
  ],
  providers: [],
  bootstrap: [ ]
})
export class AdminModule { }

