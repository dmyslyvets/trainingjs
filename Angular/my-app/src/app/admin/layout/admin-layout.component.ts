import { Component } from '@angular/core';

@Component({
    selector: 'admin-layout',
    templateUrl: './admin-layout.component.html',
    styleUrls: ['./admin-layout.component.scss']
  })

  export class AdminLayoutComponent{
      
     
    opened: boolean = true;
  
    shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));

    public closeSidebar()
    {
      this.opened = false
    }
  }