import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layout/admin-layout.component';
import { ClientComponent } from './pages/client/client.component';
import { ProductComponent } from './pages/product/product.component';
 
 

const routes: Routes = [
    
  
    {
      path: '',
      component: AdminLayoutComponent,
      children: [
        {path: '', redirectTo: 'client'},
        {
          path: 'client',
          component: ClientComponent,
        },
        {
          path: 'product',
          component: ProductComponent,
        } 
        
      ]
    },

    

  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AdminRoutingModule { }
  