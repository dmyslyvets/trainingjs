import { Component } from "@angular/core";
import { ProductService } from '../services/product-service';
import { Product } from '../models/product/product';
import { NewProduct } from '../models/product/new-product';
import { Router } from "@angular/router";

 

@Component({
    selector: "my-table",
    templateUrl: "table.component.html",
    styleUrls: ["table.component.css"]
})
export class TableComponent {
    products: Product[];
    done: boolean = false;
    public newProduct: NewProduct;

    constructor(public productService: ProductService, private router: Router) {
            this.products = new Array<Product>();
            this.newProduct = new NewProduct();
            this.getProducts();
    } 

    public addProduct()
    {
        this.productService.create(this.newProduct).subscribe(res => {
            this.getProducts();
        });
    }

    public delete(id: string) 
    {
        this.productService.delete(id)
        .subscribe((deleted) => {
            this.getProducts();
        });
    }

    public updateProduct(product: Product) {
        this.router.navigate(["models", "edit", product.id]);
    }

    public refreshTable()
    {
        this.getProducts();
    }

    getProducts()
    {
        this.productService.getAll().subscribe((items: Product[])  =>{this.products = items;} );
    } 
}


 
 