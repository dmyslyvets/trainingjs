import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ProductService } from '../services/product-service';
import { Product } from '../models/product/product';

@Component({
  selector: 'app-model-edit-component',
  templateUrl: './model-edit-component.component.html',
  styleUrls: ['./model-edit-component.component.css']
})
export class ModelEditComponent implements OnInit{

  editedProduct: Product = {
    id: null,
    name: null,
    description: null,
    price: 0
  }
  status: boolean = false;

  constructor(private service: ProductService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getProductFromRoute();
  }

  private getProductFromRoute() {
    this.activatedRoute.params.forEach((params: Params) => {
      let id = params["id"];
      this.service.getProduct(id).subscribe(
        product => {
          this.editedProduct = product;
        }
      )
    });
  }

  updateProduct() 
  {
      this.service.update(this.editedProduct).subscribe(res => {
           this.status = true;
           this.goBack();
      });
  }

  goBack(){
    this.router.navigate(["/models"]);
  }
}
