export class NewProduct {
    name: string;
    description: string;
    price: number;
}