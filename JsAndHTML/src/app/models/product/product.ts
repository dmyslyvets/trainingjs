export class Product {
    id?: string;
    creationDate?: Date;
    name: string;
    description: string;
    price: number;
}