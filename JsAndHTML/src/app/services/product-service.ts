import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from "../models/product/product";
import { NewProduct } from '../models/product/new-product';
import { environment } from 'src/environments/environment'; 

@Injectable()
export class ProductService {
     
    private apiUrl = environment.apiUrl;
    constructor(private http: HttpClient) { }

    public getAll() : Observable<Product[]> {
        return this.http.get<Product[]>(this.apiUrl + "/api/Product/getAll");
    }

    public create(product: NewProduct): Observable<string> {
        return this.http.post<string>(this.apiUrl + "/api/Product/create", product);
    }
    
    public delete(id: string): Observable<string> {
        return this.http.delete<string>(this.apiUrl +`/api/Product/delete?id=${id}`);
    }

    public update(product: Product): Observable<string> {
        return this.http.put<string>(this.apiUrl + "/api/Product/update", product);
    }

    public getProduct(id: string): Observable<Product> {
        return this.http.get<Product>(this.apiUrl + `/api/Product/get?id=${id}`);
    }

}