import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { AppComponent } from './app.component';
import { TableComponent } from './table-component/table.component';
import { ProductService } from './services/product-service';
import { HttpClientModule } from '@angular/common/http';
import { ModelEditComponent } from './model-edit-component/model-edit-component.component';
import { routes } from "./app.routes";

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    ModelEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
