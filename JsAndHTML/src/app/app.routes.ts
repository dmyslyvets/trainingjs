import { Routes } from "@angular/router";
import { TableComponent } from "./table-component/table.component";
import { ModelEditComponent } from "./model-edit-component/model-edit-component.component"


export const routes: Routes = [
    {
        path: "",
        redirectTo: "models",
        pathMatch: "full"
    },
    { path: "models", component: TableComponent },
    { path: "models/edit/:id", component: ModelEditComponent }
];